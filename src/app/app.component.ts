import { Component, OnInit } from '@angular/core';

declare let pendo: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'reservation';
  
  ngOnInit(): void {
  // in your authentication promise handler or callback
    pendo.initialize({
      visitor: {
        id:              'brighthorizons@colorado.anonaddy.com'   // Required if user is logged in
      },

      account: {
        id:           'team-c' // Highly recommended
      }
    });
  }
}

